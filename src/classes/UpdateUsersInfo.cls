public with sharing class UpdateUsersInfo {

    @InvocableMethod(Callout=true)
    public static List<String> updateUsers(List<String> data) {
        if (data != null && data[0] != null) {
            System.debug(data[0] != null);
            List<CustomUser__c> users = new List<CustomUser__c>();
            Map<String, Object> jsonMap = (Map<String, Object>) JSON.deserializeUntyped(data[0]);
            List<Object> members = (List<Object>) JSON.deserializeUntyped(JSON.serializePretty(jsonMap.get('members')));
            for (Object member : members) {
                UserWrapper userWrapper = (UserWrapper) JSON.deserialize(JSON.serializePretty(member), UserWrapper.class);
                if (userWrapper.id.toUpperCase() != 'USLACKBOT') {
                    users.add(new CustomUser__c(Name = userWrapper.name, ExternalId__c = userWrapper.id,
                            FirstName__c = userWrapper.profile.firstName,
                            LastName__c = userWrapper.profile.lastName,
                            Phone__c = userWrapper.profile.phone,
                            Skype__c = userWrapper.profile.skype,
                            Email__c = userWrapper.profile.email));
                }
            }
            upsert users ExternalId__c;
        }
        return new List<String>{
                ''
        };
    }

    public class UserWrapper {
        String id;
        String name;
        UserInfo profile;
    }

    public class UserInfo {

        String firstName;
        String lastName;
        String phone;
        String skype;
        String email;
    }

}