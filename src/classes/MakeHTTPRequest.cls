public with sharing class MakeHTTPRequest {

    @InvocableMethod(Callout=true)
    public static List<String> makeHTTP(List<RequestProperties> properties) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(properties[0].URL);
        req.setMethod(properties[0].HTTPMethod);
        if (properties[0].body!=null) {
            req.setMethod(properties[0].body);
        }
        if (properties[0].headerConfigs !=null) {
            Map<String, String > headerProperties = (Map<String, String>) JSON.deserializeStrict(properties[0].headerConfigs,Map<String, String>.class);
            for (String headerPropertyKey : headerProperties.keySet()) {
                req.setHeader(headerPropertyKey, headerProperties.get(headerPropertyKey));
            }
        }
        Http http = new Http();
        System.debug(req);
        HTTPResponse res = http.send(req);
        System.debug('res.getBody(): ' + res.getBody());
        if (res.getStatusCode()==200) {
            return new List<String>{res.getBody()};
        }
        return new List<String>{res.getStatus()};
    }



    public class RequestProperties {
        @InvocableVariable
        public String URL;

        @InvocableVariable
        public String HTTPMethod;

        @InvocableVariable
        public String timeout;

        @InvocableVariable
        public String headerConfigs;

        @InvocableVariable
        public String body;
    }

}